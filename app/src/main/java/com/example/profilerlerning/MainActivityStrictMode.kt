package com.example.profilerlerning

import android.os.Bundle
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.os.Environment.getExternalStoragePublicDirectory
import android.os.StrictMode
import androidx.appcompat.app.AppCompatActivity
import java.io.File


class MainActivityStrictMode : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //Если я вынесу эту проверку на существование файла из UI потока
        // в фоновый поток, то StrictMode также ничего мне сообщит, т.к.
        // он был настроен в UI потоке, а значит, мониторит только его.
        //Настроим StrictMode, проверка чтения с диска и в случае
        // обнаружения вывод в лог
        //гугл не рекомендует оставлять StrictMode включенным в релизном билде.

        //detectCustomSlowCalls - поможет вам отловить то, что вы сами пометите.
        // Например, у вас есть какой-то метод, про который вы точно знаете, что
        // он выполняется очень долго, и он ни в коем случае не должен вызываться
        // в UI потоке. Вы просто помещаете в этот метод вызов метода
        // StrictMode.noteSlowCall. И как только ваш долгий метод будет
        // вызван в потоке, который контролируется StrictMode с включенным
        // detectCustomSlowCalls, вы об этом узнаете.

        //Способы оповещения:
        //
        //penaltyLog - вывод в лог
        //
        //penaltyFlashScreen - экран моргнет красной рамкой. Я тестировал на эмуляторе,
        // очень редко срабатывает, не знаю почему.
        //
        //penaltyDialog - покажет вам небольшой диалог, но не чаще, чем один раз
        // в 30 сек
        //
        //penaltyDropBox - похоже на penaltyLog, но будет сохранять информацию в некое
        // локальное хранилище, которое называется dropbox (не тот Dropbox,
        // который облачное хранилище). Достать оттуда данные можно командой:
        //adb shell dumpsys dropbox data_app_strictmode --print
        //
        //penaltyDeath - крэшнет приложение
        //
        //penaltyListener (Executor executor, StrictMode.OnThreadViolationListener
        // listener) - можно повесить свой колбэк, в который будут приходить
        // оповещения. Параметр executor нужен, чтобы указать в каком потоке
        // должен вызываться ваш колбэк. Ведь StrictMode может быть настроен
        // на любой поток, а получение колбэка можно настроить на UI поток.
        if (BuildConfig.DEBUG) {
            val threadPolicy = StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .penaltyLog()
                .build()
            StrictMode.setThreadPolicy(threadPolicy)
        }

        val file = File(
            getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS),
            "file.txt"
        )
        file.exists()

        //VmPolicy
        //Что может обнаружить:
        //
        //detectActivityLeaks - мемори лики, которые держат в памяти Activity и не дают ему закрыться
        //
        //detectLeakedClosableObjects - незакрытые Closable объекты, например, FileWriter. Если вы не выполнили его обязательный метод close(), то StrictMode это обнаружит
        //
        //detectCleartextNetwork - данные передаваемые в сеть не защищены SSL/TLS
        //
        //detectLeakedSqlLiteObjects - незакрытые SQLite курсоры
        //
        //detectAll - включит все проверки сразу.
        //
        //setClassInstanceLimit(Class klass, int instanceLimit) - очень полезный метод. Он позволяет указать максимальное количество объектов определенного класса, которые могут находиться в памяти. Если приложение в своей работе создало больше объектов, чем вы указали, StrictMode это обнаружит и выдаст информация следующего вида:

    }




}
