package com.example.profilerlerning

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.profilerlerning.model.A
import com.example.profilerlerning.model.C
import com.example.profilerlerning.model.MyObj
import kotlinx.android.synthetic.main.activity_memory_profiler.*


class ActivityMemoryProfilerAllocation : AppCompatActivity() {

    private var mutableList: MutableList<MyObj>? = mutableListOf()

    lateinit var a: A
    lateinit var c: C

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memory_profiler)

//        btnCreateObj.setOnClickListener {
//            Thread {
//                for (index in 0..10) {
//                    mutableList?.add(MyObj())
//                }
//            }.run()
//        }
//
//        btnCleareObj.setOnClickListener {
//            mutableList?.forEach {
//                it.clear()
//            }
//        }
        btnCreateObj.setOnClickListener {
            a = A()
            c = C()

            //как сделать дамп файла
//            val dumpFile = File(
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
//                "dump.hprof"
//            )
//            try {
//                Debug.dumpHprofData(dumpFile.absolutePath)
//            } catch (e1: IOException) {
//                e1.printStackTrace()
//            }
        }
    }


}