package com.example.profilerlerning

import android.app.ActivityManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity


class ActivityMemory : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Android может предоставить нам информацию о текущем состоянии
        // памяти как всего устройства, так и нашего приложения.
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val memoryInfo = ActivityManager.MemoryInfo()
        activityManager.getMemoryInfo(memoryInfo)

        //memoryInfo.totalMem - сколько всего памяти имеет устройство

        //memoryInfo.availMem - сколько памяти свободно

        //memoryInfo.threshold - если свободной памяти останется меньше,
        // чем это значение, система перейдет в режим low memory и начнет
        // просить приложения освобождать ресурсы, а потом и закрывать
        // процессы, если станет совсем плохо.

        //memoryInfo.lowMemory - boolean флаг, означающий, что система
        // находится в режиме low memory. Его стоит проверять перед
        // тем, как мы собираемся делать что-то требующее много памяти.
        Log.d("ActivityMemory", "сколько всего памяти имеет устройство: ${memoryInfo.totalMem / 1024 / 1024}")
        Log.d("ActivityMemory", "сколько памяти свободно: ${memoryInfo.availMem/ 1024 / 1024}")
        Log.d("ActivityMemory", "до режима low memory: ${(memoryInfo.availMem - memoryInfo.threshold) / 1024 / 1024}")

        //Информация о текущем состоянии памяти приложения:
        //НЕ РАБОТАЕТ
        val runtime = Runtime.getRuntime()
        val totalMemory = runtime.totalMemory() / 1024 / 1024
        val freeMemory = runtime.freeMemory() / 1024 / 1024
        val usedMemory = totalMemory - freeMemory

        val maxMemory = runtime.maxMemory() / 1024 / 1024
        val availableMemory = maxMemory - usedMemory
        Log.d("ActivityMemory", "totalMemory: ${totalMemory / 1024 / 1024}")
        Log.d("ActivityMemory", "freeMemory: ${freeMemory/ 1024 / 1024}")
        Log.d("ActivityMemory", "usedMemory: ${usedMemory / 1024 / 1024}")
        Log.d("ActivityMemory", "maxMemory: ${maxMemory / 1024 / 1024}")
        Log.d("ActivityMemory", "availableMemory: ${availableMemory / 1024 / 1024}")
    }


    //В режиме low memory система просит приложения освободить
    // ненужные ресурсы и объекты. Для этого она вызывает метод
    // onTrimMemory, который есть во всех основных компонентах
    // системы (Activity, Fragment, Service, ContentProvider и Application)
    /**
     * @param level отражает, насколько критична ситуация с памятью.
     * TRIM_MEMORY_RUNNING_MODERATE (5) - можно освободить что-нибудь ненужное
     * TRIM_MEMORY_RUNNING_LOW (10) - следует освободить что-нибудь ненужное
     * TRIM_MEMORY_RUNNING_CRITICAL (15) - следует освободить
     * все возможные некритические ресурсы, потому что система уже
     * готова начать закрывать приложения, которые висят в фоне (background).
     *
     * Когда приложение переходит в фон и его UI больше не виден, в метод onTrimMemory приходит level:
     * TRIM_MEMORY_UI_HIDDEN (20) Это значит, что следует освободить ресурсы, которые использовались в UI. Например - картинки из ImageView.
     *
     * TRIM_MEMORY_BACKGROUND (40) - приложению пока не грозит быть закрытым, но уже пора освобождать ресурсы, которые быстро могут быть восстановлены, когда приложение вернется из фона
     * TRIM_MEMORY_MODERATE (60) - приложение уже в середине очереди на закрытие.
     * TRIM_MEMORY_COMPLETE (80) - приложение одно из первых в очереди на закрытие. Надо освободить как можно больше ресурсов не являющихся критичными для возврата приложения из фона.
     */
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        // ... освобождаем ресурсы
        //Документация рекомендует нам использовать не =, а >= при работе
        // с этими константами, потому что в дальнейшем могут быть добавлены
        // новые значения.

    }

}