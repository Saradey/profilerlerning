package com.example.profilerlerning

import android.app.Application
import com.example.profilerlerning.memory.leak.Repository
import com.example.profilerlerning.memory.leak.RxJavaMemoryLeak

class MainApp : Application() {

    val repository = Repository()
    val rxRepository = RxJavaMemoryLeak()


    override fun onCreate() {
        super.onCreate()
    }


}