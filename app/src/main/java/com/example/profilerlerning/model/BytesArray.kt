package com.example.profilerlerning.model

open class BytesArray(
    count: Int
) {

    private var bytes: Array<Byte> = Array(count * 1000 * 1000) { 1.toByte() }

}