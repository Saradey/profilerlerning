package com.example.profilerlerning.model

class MyObj {

    var list: Array<Byte>? = Array(1000 * 1000) { 1.toByte() }

    fun clear() {
        list = null
    }

}