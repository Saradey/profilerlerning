package com.example.profilerlerning.memory.leak

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.profilerlerning.MainApp
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

class ActivityMemoryLeak : AppCompatActivity(), Repository.RepositoryListener {


    lateinit var bytes: Array<Byte>

    var disposables: CompositeDisposable = CompositeDisposable()


    //утечка памяти при статике
//    companion object {
//        @JvmStatic
//        lateinit var context: Context
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bytes = Array(1000 * 1000 * 20) { 1.toByte() }

        //А Consumer (как anonymous class) будет держать скрытую ссылку на TestActivity.
        (applicationContext as MainApp).repository.addListener(this)
//        (applicationContext as MainApp).rxRepository.observableInterval.subscribe(object :
//            Consumer<Long> {
//            override fun accept(it: Long?) {
//                this@ActivityMemoryLeak.showLogD(it ?: 0)
//            }
//        })

//        решение:

//        disposables += (applicationContext as MainApp).rxRepository.observableInterval.subscribe { it ->
//            this@ActivityMemoryLeak.showLogD(it ?: 0)
//        }

//        context = this

        
    }


    private fun showLogD(it: Long) {
        Log.d("Timer", "$it")
    }


    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    this.addAll(disposable)
}