package com.example.profilerlerning.memory.leak

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.profilerlerning.R
import kotlinx.android.synthetic.main.activity_main_memory_leak.*

class MainActivityMemoryLeak : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_memory_leak)

        btnGoToNextActivity.setOnClickListener {
            startActivity(Intent(this, ActivityMemoryLeak::class.java))
        }
    }


}