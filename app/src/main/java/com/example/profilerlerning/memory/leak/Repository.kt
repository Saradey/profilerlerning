package com.example.profilerlerning.memory.leak

import android.content.Context
import java.lang.ref.WeakReference

class Repository(
) {

    //    private val listenerSet: HashSet<WeakReference<RepositoryListener>> = hashSetOf()
//    private lateinit var context: Context
//    private lateinit var listener: WeakReference<RepositoryListener>
    private val listenerSet: HashSet<RepositoryListener> = hashSetOf()

    interface RepositoryListener


    fun addListener(repository: RepositoryListener) {
//        context = (repository as ActivityMemoryLeak).applicationContext
        listenerSet.add(repository)
//        listener = WeakReference(repository)
    }

}